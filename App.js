/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import RNSpatial from '@mvits/react-native-spatial';


// Ejecutar la conexión a la base de datos

//return db.executeQuery('SELECT * FROM MyTable');

const App: () => React$Node = () => {

  let dbParams = {
    dbName: 'testingDB.sqlite'
  };

  RNSpatial.connect(dbParams);
  RNSpatial.executeQuery("SELECT  spatialite_version();")
            .then(response => {
                console.log(response);
            });

  RNSpatial.executeQuery("SELECT  rttopo_version();")
            .then(response => {
                console.log(response);
            });

  RNSpatial.executeQuery('SELECT AsGeoJSON(Split((GeomFromGeoJSON(\'{"type":"Polygon","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[[800160.1765726774,1155825.344210001],[800165.6288166349,1155825.075626555],[800164.2053243701,1155818.692293317],[800160.0601865174,1155819.506996437],[800160.1765726774,1155825.344210001]]]}\')),(GeomFromGeoJSON(\'{"type":"LineString","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[800164.7685470553,1155825.743379492],[800161.9396545551,1155822.019633241],[800165.4324708053,1155818.642281991]]}\'))),50,2) as geometry;')
            .then(response => {
                console.log(response);
            });


  RNSpatial.executeQuery('SELECT AsGeoJSON(SplitLeft((GeomFromGeoJSON(\'{"type":"Polygon","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[[800160.1765726774,1155825.344210001],[800165.6288166349,1155825.075626555],[800164.2053243701,1155818.692293317],[800160.0601865174,1155819.506996437],[800160.1765726774,1155825.344210001]]]}\')),(GeomFromGeoJSON(\'{"type":"LineString","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[800164.7685470553,1155825.743379492],[800161.9396545551,1155822.019633241],[800165.4324708053,1155818.642281991]]}\'))),50,2) as geometry;')
            .then(response => {
                console.log(response);
            });

  RNSpatial.executeQuery('SELECT AsGeoJSON(SplitRight((GeomFromGeoJSON(\'{"type":"Polygon","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[[800160.1765726774,1155825.344210001],[800165.6288166349,1155825.075626555],[800164.2053243701,1155818.692293317],[800160.0601865174,1155819.506996437],[800160.1765726774,1155825.344210001]]]}\')),(GeomFromGeoJSON(\'{"type":"LineString","crs":{"type":"name","properties":{"name":"EPSG:3116"}},"coordinates":[[800164.7685470553,1155825.743379492],[800161.9396545551,1155822.019633241],[800165.4324708053,1155818.642281991]]}\'))),50,2) as geometry;')
            .then(response => {
                console.log(response);
            });

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
          <View style={styles.body}>

          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
